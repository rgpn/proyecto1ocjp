package ocjpClases;

/**
 * Created by july on 13/06/2017.
 */
public class Cuenta {
    private String numeroCta;
    private double saldoInicial;
    private double saldoActual;

    public Cuenta(String numeroCta, double saldoInicial, double saldoActual) {
        this.setNumeroCta(numeroCta);
        this.setSaldoInicial(saldoInicial);
        this.setSaldoActual(saldoActual);
    }

    public String getNumeroCta() {
        return numeroCta;
    }

    public void setNumeroCta(String numeroCta) {
        this.numeroCta = numeroCta;
    }

    public double getSaldoInicial() {
        return saldoInicial;
    }

    public void setSaldoInicial(double saldoInicial) {
        this.saldoInicial = saldoInicial;
    }

    public double getSaldoActual() {
        return saldoActual;
    }

    public void setSaldoActual(double saldoActual) {
        this.saldoActual = saldoActual;
    }

    public String toString() {
        return getNumeroCta() + " " + getSaldoInicial() + " " + getSaldoActual();
    }
}
