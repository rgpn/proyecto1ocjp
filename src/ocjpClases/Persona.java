package ocjpClases;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by visitante on 13/06/2017.
 */
public class Persona {

    private int id;
    private String nombre;
    private byte edad;
    private Date fechaNacimiento;
    private String email;
    private char PAIS = 'E';

    /**
     * Constructor de la clase persona
     * @param id El id de la persona
     * @param nombre El nombre de la persona
     * @param edad La edad de la persona
     * @param fechaNacimiento La fecha de nacimiento de la persona
     * @param email El correo electronico de la persona
     */
    public Persona(int id, String nombre, byte edad, String fechaNacimiento, String email) {
        this.setId(id);
        this.setNombre(nombre);
        this.setEdad(edad);
        this.setFechaNacimiento(fechaNacimiento);
        this.setEmail(email);
    }

    /**
     * Getters and Setters
     */
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public byte getEdad() {
        return edad;
    }

    public void setEdad(byte edad) {
        this.edad = edad;
    }

    @SuppressWarnings("deprecation")
    public String getFechaNacimiento() {
        int day = fechaNacimiento.getDate();
        int month = fechaNacimiento.getMonth() + 1;
        int year = fechaNacimiento.getYear() + 1900;
        String auxDay = "" + day;
        String auxMonth = "" + month;
        if(day<10)
            auxDay = "0" + day;
        if(month<10)
            auxMonth = "0" + month;
        String fechaNacimiento = auxDay + "/" + auxMonth + "/" + year;
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        try {
            this.fechaNacimiento = df.parse(fechaNacimiento);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public char getPAIS() {
        return PAIS;
    }

    public String toString() {
        return getId() + " " + getNombre() + " " + getEdad() + " " + getFechaNacimiento()
                + " " + getEmail() + " " + getPAIS();
    }
}