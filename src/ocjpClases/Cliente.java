package ocjpClases;

/**
 * Created by july on 13/06/2017.
 */
public class Cliente extends Persona {
    private String celular;
    private double sueldo;
    private String tipoCuenta;

    static String dato="Datos del cliente";
    //implementacion de InnerClass

    static class clientesInner{
        static void msg(){System.out.println(dato);}
    }

    public Cliente(int id, String nombre, byte edad, String fechaNacimiento, String email, String celular, double sueldo, String tipoCuenta) {



        super(id, nombre, edad, fechaNacimiento, email);
        Cliente.clientesInner.msg();
        this.celular = celular;
        this.sueldo = sueldo;
        this.tipoCuenta = tipoCuenta;

    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public double getSueldo() {
        return sueldo;
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }

    public String getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }
}
