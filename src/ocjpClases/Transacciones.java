package ocjpClases;

        import java.awt.event.KeyAdapter;
        import java.awt.event.KeyEvent;
        import java.util.List;

        import javax.swing.JFrame;
        import javax.swing.JLabel;
        import javax.swing.JOptionPane;
        import javax.swing.JTextField;


public class Transacciones {

    public JFrame frmTransacciones;
    private JFrame frmMenu;
    private List<Cuenta> listaCuentas;
    private JTextField txtOpcion;

    /**
     * Create the application.
     */
    public Transacciones(JFrame frmMenu, List<Cuenta> listaCuentas) {
        this.frmMenu = frmMenu;
        this.listaCuentas = listaCuentas;
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmTransacciones = new JFrame();
        frmTransacciones.setTitle("Transacciones");
        frmTransacciones.setResizable(false);
        frmTransacciones.setBounds(100, 100, 240, 145);
        frmTransacciones.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmTransacciones.getContentPane().setLayout(null);

        JLabel lblDeposito = new JLabel("1. Deposito");
        lblDeposito.setBounds(10, 11, 100, 14);
        frmTransacciones.getContentPane().add(lblDeposito);

        JLabel lblRetiro = new JLabel("2. Retiro");
        lblRetiro.setBounds(10, 36, 100, 14);
        frmTransacciones.getContentPane().add(lblRetiro);

        JLabel lblRegresar = new JLabel("3. Regresar");
        lblRegresar.setBounds(10, 61, 100, 14);
        frmTransacciones.getContentPane().add(lblRegresar);

        JLabel lblElijaUnaOpcion = new JLabel("Elija una opcion:");
        lblElijaUnaOpcion.setBounds(10, 86, 100, 14);
        frmTransacciones.getContentPane().add(lblElijaUnaOpcion);

        txtOpcion = new JTextField();
        txtOpcion.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_ENTER)
                    opcion(txtOpcion.getText());
                if(e.getKeyCode() == KeyEvent.VK_ESCAPE)
                    System.exit(0);
            }
        });
        txtOpcion.setBounds(120, 83, 86, 20);
        frmTransacciones.getContentPane().add(txtOpcion);
        txtOpcion.setColumns(10);
    }
    /**
     * Evalua que opcion del menu ejecutar.
     * @param opcion Opcion del menu.
     */
    private void opcion(String opcion) {
        int op = 0;

        try {
            op = Integer.parseInt(opcion);
            if(opcion.matches("[1-3]")) {
                switch(op) {
                    case 1:
                        deposito();
                        break;
                    case 2:
                        retiro();
                        break;
                    case 3:
                        back();
                }
            } else {
                JOptionPane.showMessageDialog(null, "Ingrese una opcion valida", "Warning", JOptionPane.WARNING_MESSAGE);
                txtOpcion.setText(null);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e + "\nDebe ingresar numeros", "Error", JOptionPane.ERROR_MESSAGE);
            txtOpcion.setText(null);
        }
    }

    /**
     * Inicializa la pantalla de deposito.
     */
    private void deposito() {
        txtOpcion.setText(null);
        frmTransacciones.setVisible(false);
        Deposito window = new Deposito(frmTransacciones, listaCuentas);
        window.frmDeposito.setVisible(true);
    }

    /**
     * Inicializa la pantalla de retiro.
     */
    private void retiro() {
        txtOpcion.setText(null);
        frmTransacciones.setVisible(false);
        Retiro window = new Retiro(frmTransacciones, listaCuentas);
        window.frmRetiro.setVisible(true);
    }

    /**
     * Regresa a la pantalla anterior.
     */
    private void back() {
        frmTransacciones.setVisible(false);
        frmMenu.setVisible(true);
    }

}
