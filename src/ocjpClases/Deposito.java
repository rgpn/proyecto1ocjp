package ocjpClases;

/**
 * Created by Wilmer on 14/06/2017.
 */
import ocjpExcepciones.MyException;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.*;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import java.util.List;

import javax.swing.JFrame;
public class Deposito {
    public JFrame frmDeposito;
    private JFrame frmTransacciones;
    private List<Cuenta> listaCuentas;
    private JTextField txtSaldoActual;
    private JTextField txtMontoADepositar;
    Button boton;
    @SuppressWarnings("rawtypes")
    private JComboBox cmbNroCuenta;

    /**
     * Create the application.
     */
    public Deposito(JFrame frmTransacciones, List<Cuenta> listaCuentas) {
        this.frmTransacciones = frmTransacciones;
        this.listaCuentas = listaCuentas;
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private void initialize() {
        frmDeposito = new JFrame();
        frmDeposito.setTitle("Deposito");
        frmDeposito.setResizable(false);
        frmDeposito.setBounds(100, 100, 245, 145);
        frmDeposito.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmDeposito.getContentPane().setLayout(null);



        JLabel lblNroCuenta = new JLabel("Numero de Cuenta");
        lblNroCuenta.setBounds(10, 11, 120, 14);
        frmDeposito.getContentPane().add(lblNroCuenta);

        JLabel lblSaldoActual = new JLabel("Saldo Actual");
        lblSaldoActual.setBounds(10, 36, 120, 14);
        frmDeposito.getContentPane().add(lblSaldoActual);

        JLabel lblMontoADepositar = new JLabel("Monto a Depositar");
        lblMontoADepositar.setBounds(10, 61, 120, 14);
        frmDeposito.getContentPane().add(lblMontoADepositar);

        txtSaldoActual = new JTextField();
        txtSaldoActual.setEditable(false);
        txtSaldoActual.setBounds(140, 33, 86, 20);
        frmDeposito.getContentPane().add(txtSaldoActual);
        txtSaldoActual.setColumns(10);

        txtMontoADepositar = new JTextField();
        txtMontoADepositar.setBounds(140, 58, 86, 20);
        frmDeposito.getContentPane().add(txtMontoADepositar);
        txtMontoADepositar.setColumns(10);

        String[] cuentas = new String[listaCuentas.size()];
        for(int i = 0; i < listaCuentas.size(); i++)
            cuentas[i] = listaCuentas.get(i).getNumeroCta();

        cmbNroCuenta = new JComboBox(cuentas);
        cmbNroCuenta.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent arg0) {
                llenarCampos();

            }
        });
        cmbNroCuenta.setBounds(140, 8, 86, 20);
        frmDeposito.getContentPane().add(cmbNroCuenta);

        JButton btnAceptar = new JButton("Aceptar");
        btnAceptar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                depositar();
                /**
                 * casting
                 */
                System.out.println("entro a castign");
                boolean bandera=false;
                try {
                    boton=(Button)frmDeposito.getContentPane().getComponent(0);
                    bandera=true;
                    assert bandera : "valid casting";

                    System.out.println(bandera);

                }catch (Exception e){

                        assert bandera : "invalid casting";
                        System.out.println(bandera);

                }


            }
        });
        btnAceptar.setBounds(10, 86, 89, 23);
        frmDeposito.getContentPane().add(btnAceptar);

        JButton btnCancelar = new JButton("Cancelar");
        btnCancelar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                back();
            }
        });
        btnCancelar.setBounds(137, 86, 89, 23);
        frmDeposito.getContentPane().add(btnCancelar);
    }
    private void depositar() {
        double monto = 0;
        try {
            monto = Double.parseDouble(txtMontoADepositar.getText());
            assert monto<200 : "monto incorrect";

        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "\nDebe ingresar un numero", "Error", JOptionPane.ERROR_MESSAGE);
        }
        int key = cmbNroCuenta.getSelectedIndex();

/*        try {
            deposito(monto);
            listaCuentas.get(key).setSaldoActual(listaCuentas.get(key).getSaldoActual() + monto);
            txtMontoADepositar.setText(null);
            llenarCampos();
            JOptionPane.showMessageDialog(null, "\nTransaccion realizada con Exito", "Resultado", JOptionPane.INFORMATION_MESSAGE);

        } catch (MyException.ExcDeposito ex) {

            JOptionPane.showMessageDialog(null,ex.getMessage());

        }
    }*/
        if(monto > 0) {
            listaCuentas.get(key).setSaldoActual(listaCuentas.get(key).getSaldoActual() + monto);
            txtMontoADepositar.setText(null);
            llenarCampos();
            JOptionPane.showMessageDialog(null, "\nTransaccion realizada con Exito", "Resultado", JOptionPane.INFORMATION_MESSAGE);
        }
        else
            JOptionPane.showMessageDialog(null, "El monto a depositar debe ser mayor a 0", "Warning", JOptionPane.WARNING_MESSAGE);
    }

    static void deposito(double a) throws MyException.ExcDeposito {
        if ((a>0)&&(a<=200)) {
            throw new MyException.ExcDeposito("El monto debe ser mayor a 0 y menor 200");
        }
    }
    private void llenarCampos() {
        int key = cmbNroCuenta.getSelectedIndex();
        txtSaldoActual.setText(String.valueOf(listaCuentas.get(key).getSaldoActual()));
        System.out.println("key="+key+"ListaCuentas: \n"+listaCuentas.toString());
    }
    private void back() {
        frmDeposito.setVisible(false);
        frmTransacciones.setVisible(true);
    }
}
