package ocjpClases;

/**
 * Created by Wilmer on 14/06/2017.
 */
import ocjpExcepciones.MyException;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import java.util.List;

import javax.swing.JFrame;
public class Retiro {
    public JFrame frmRetiro;
    private JFrame frmTransacciones;
    private List<Cuenta> listaCuentas;
    private JTextField txtSaldoActual;
    private JTextField txtMontoAretirar;
    @SuppressWarnings("rawtypes")
    private JComboBox cmbNroCuenta;

    /**
     * Create the application.
     */
    public Retiro(JFrame frmTransacciones, List<Cuenta> listaCuentas) {
        this.frmTransacciones = frmTransacciones;
        this.listaCuentas = listaCuentas;
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    private void initialize() {
        frmRetiro = new JFrame();
        frmRetiro.setTitle("Retiro");
        frmRetiro.setResizable(false);
        frmRetiro.setBounds(100, 100, 245, 145);
        frmRetiro.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmRetiro.getContentPane().setLayout(null);

        JLabel lblNroCuenta = new JLabel("Numero de Cuenta");
        lblNroCuenta.setBounds(10, 11, 120, 14);
        frmRetiro.getContentPane().add(lblNroCuenta);

        JLabel lblSaldoActual = new JLabel("Saldo Actual");
        lblSaldoActual.setBounds(10, 36, 120, 14);
        frmRetiro.getContentPane().add(lblSaldoActual);

        JLabel lblMontoARetirar = new JLabel("Monto a Retirar");
        lblMontoARetirar.setBounds(10, 61, 120, 14);
        frmRetiro.getContentPane().add(lblMontoARetirar);

        txtSaldoActual = new JTextField();
        txtSaldoActual.setEditable(false);
        txtSaldoActual.setBounds(140, 33, 86, 20);
        frmRetiro.getContentPane().add(txtSaldoActual);
        txtSaldoActual.setColumns(10);

        txtMontoAretirar = new JTextField();
        txtMontoAretirar.setBounds(140, 58, 86, 20);
        frmRetiro.getContentPane().add(txtMontoAretirar);
        txtMontoAretirar.setColumns(10);

        String[] cuentas = new String[listaCuentas.size()];
        for (int i = 0; i < listaCuentas.size(); i++)
            cuentas[i] = listaCuentas.get(i).getNumeroCta();

        cmbNroCuenta = new JComboBox(cuentas);
        cmbNroCuenta.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent arg0) {
                llenarCampos();

            }
        });
        cmbNroCuenta.setBounds(140, 8, 86, 20);
        frmRetiro.getContentPane().add(cmbNroCuenta);

        JButton btnAceptar = new JButton("Aceptar");
        btnAceptar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                retirar();

            }
        });
        btnAceptar.setBounds(10, 86, 89, 23);
        frmRetiro.getContentPane().add(btnAceptar);

        JButton btnCancelar = new JButton("Cancelar");
        btnCancelar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                back();
            }
        });
        btnCancelar.setBounds(137, 86, 89, 23);
        frmRetiro.getContentPane().add(btnCancelar);
    }

    private void retirar() {
        /*Classe Wrapper
        * */
        double monto = 0;
        double saldoA = Double.parseDouble(txtSaldoActual.getText());

        try {
            monto = Double.parseDouble(txtMontoAretirar.getText());
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "\nDebe ingresar un numero", "Error", JOptionPane.ERROR_MESSAGE);
        }
        int key = cmbNroCuenta.getSelectedIndex();

        try {
            retiro(saldoA, monto);
            listaCuentas.get(key).setSaldoActual(listaCuentas.get(key).getSaldoActual() - monto);
            txtMontoAretirar.setText(null);
            llenarCampos();
            JOptionPane.showMessageDialog(null, "\nTransaccion realizada con Exito", "Resultado", JOptionPane.INFORMATION_MESSAGE);
        } catch (MyException.ExcRetiro e) {

            JOptionPane.showMessageDialog(null,e.getMessage());
        }
    }
        /*if (saldoA > monto) {
            listaCuentas.get(key).setSaldoActual(listaCuentas.get(key).getSaldoActual() - monto);
            txtMontoAretirar.setText(null);
            llenarCampos();
        } else
            JOptionPane.showMessageDialog(null, "El monto a Retirar debe ser menor al Saldo Actual", "Warning", JOptionPane.WARNING_MESSAGE);
    }
*/

    private void llenarCampos() {

        /**
         * Classe Wrapper
         */
        
        Integer ky=new Integer(cmbNroCuenta.getSelectedIndex());
        int key = ky.intValue();
        txtSaldoActual.setText(String.valueOf(listaCuentas.get(key).getSaldoActual()));
        System.out.println("key=" + key + "ListaCuentas: \n" + listaCuentas.toString());
    }

    private void back() {
        frmRetiro.setVisible(false);
        frmTransacciones.setVisible(true);
    }

    static void retiro(double a, double b) throws MyException.ExcRetiro {
        if (a < b) {
            throw new MyException.ExcRetiro("No puede retirar un monto mayor al saldo actual");
        }
    }
}
