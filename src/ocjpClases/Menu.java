package ocjpClases;

/**
 * Created by Wilmer on 14/06/2017.
 */


        import java.awt.event.KeyAdapter;
        import java.awt.event.KeyEvent;
        import java.util.List;

        import javax.swing.JFrame;
        import javax.swing.JLabel;
        import javax.swing.JOptionPane;
        import javax.swing.JTextField;

public class Menu {

    public JFrame frmMenu;
    private JTextField txtOpcion;
    private List<Cuenta> listaCuentas;
    private List<Cliente> listaClientes;

    /**
     * Create the application.
     */
    public Menu(List<Cuenta> listaCuentas, List<Cliente> listaClientes) {
        this.listaCuentas = listaCuentas;
        this.listaClientes = listaClientes;
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmMenu = new JFrame();
        frmMenu.setResizable(false);
        frmMenu.setTitle("Menu");
        frmMenu.setBounds(100, 100, 225, 140);
        frmMenu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmMenu.getContentPane().setLayout(null);

        JLabel lblAdministracion = new JLabel("1. Transacciones");
        lblAdministracion.setBounds(10, 11, 100, 14);
        frmMenu.getContentPane().add(lblAdministracion);

        JLabel lblTransacciones = new JLabel("2. salir");
        lblTransacciones.setBounds(10, 36, 100, 14);
        frmMenu.getContentPane().add(lblTransacciones);



        JLabel lblElijaUnaOpcion = new JLabel("Elija una opcion:");
        lblElijaUnaOpcion.setBounds(10, 86, 100, 14);
        frmMenu.getContentPane().add(lblElijaUnaOpcion);

        txtOpcion = new JTextField();
        txtOpcion.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_ENTER)
                    opcion(txtOpcion.getText());
                if(e.getKeyCode() == KeyEvent.VK_ESCAPE)
                    System.exit(0);
            }
        });
        txtOpcion.setBounds(120, 83, 86, 20);
        frmMenu.getContentPane().add(txtOpcion);
        txtOpcion.setColumns(10);
    }

    /**
     * Evalua que opcion del menu ejecutar.
     * @param opcion Opcion del menu.
     */
    private void opcion(String opcion) {
        int op = 0;

        try {
            op = Integer.parseInt(opcion);
            if(opcion.matches("[1-2]")) {
                switch(op) {

                    case 1:
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                transacciones();
                            }
                        }).start();
                        break;
                    case 2:
                        System.exit(0);
                }
            }
            else {
                JOptionPane.showMessageDialog(null, "Ingrese una opcion valida", "Warning", JOptionPane.WARNING_MESSAGE);
                txtOpcion.setText(null);
            }
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, e + "\nDebe ingresar numeros", "Error", JOptionPane.ERROR_MESSAGE);
            txtOpcion.setText(null);
        }
    }

    /**
     * Inicializa la pantalla de administracion.
     */


    /**
     * Inicializa la pantalla de transacciones.
     */
    private void transacciones() {
        txtOpcion.setText(null);
        //frmMenu.setVisible(false);
        Transacciones window = new Transacciones(frmMenu, listaCuentas);
        window.frmTransacciones.setVisible(true);
    }
}
