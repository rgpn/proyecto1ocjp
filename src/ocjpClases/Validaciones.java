package ocjpClases;

import ocjpExcepciones.MyException;

import java.util.List;

/**
 * Created by Wilmer on 14/06/2017.
 */
public class Validaciones {

 /* Valida que el id sea unico.
            * @param listaClientes Lista en la que va a comparar el id.
            * @param id El objeto a ser comparado.
            * @return Si el id esta diponible retorna true caso contrario arroja una excepcion.
	 * @throws IdException Arroja una excepcion del tipo IdException.
	 */
    public static boolean validarIdCliente(List<Cliente> listaClientes, int id) throws MyException.IdException {
        for(int i = 0; i < listaClientes.size(); i++)
            if(id == listaClientes.get(i).getId())
                throw new MyException.IdException("Ese id ya existe");
        return true;
    }

}
