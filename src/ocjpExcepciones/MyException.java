package ocjpExcepciones;

/**
 * Created by USER on 14/06/2017.
 */
public class MyException {

 public static class  ExcRetiro extends Exception{
        public ExcRetiro(String msg){
            super(msg);
        }
    }

    public static class  ExcDeposito extends Exception{
        public ExcDeposito(String msg){
            super(msg);
        }
    }

    public static class IdException extends Exception{
    public IdException(String errorMsg) {
        super(errorMsg);
    }
    }

}
