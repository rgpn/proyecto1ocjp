package com.company;

import ocjpClases.Cliente;
import ocjpClases.Cuenta;
import ocjpClases.TipoCuenta;


import java.util.ArrayList;
import java.util.List;

/**
 * Esta clase contiene los datos de cuenta y clientes
 * @author Jonathan Pachacama
 * @version 13-06-2017
 */


public class Datos {

    static String dato="Lista de las cuentas";
    //implementacion de InnerClass

    static class datosInner{
        static void msg(){System.out.println(dato);}
    }


    //se crea una lista de Cuenta con datos quemados

    public List<Cuenta> listaCuentas() {

        Datos.datosInner.msg();//no se necesita crear la instacia de l static nested class
        List<Cuenta> listaCuentas = new ArrayList<>();

        for(int i=1;i<10;i++) {

            String numeroCuenta = "000.0000"+ Integer.toString(i);  //imprimir correctamente el numero de cuenta
            double saldoInicial = 100+i;
            double saldoActual = 200+saldoInicial;
            Cuenta cuenta = new Cuenta(numeroCuenta, saldoInicial, saldoActual);
            listaCuentas.add(cuenta);
            System.out.println(cuenta.toString());
        }
        return listaCuentas;
    }

    //Creacion lista clientes con datos quemados
    public List<Cliente> listaClientes() {
        List<Cliente> listaCliente = new ArrayList<>();

        for(int i=1;i<10;i++) {


            String celular =i+ "099234751";
            /**
             classes Wrapper
             */

            Double sueldo;
            double  suel = i + 1000;
            sueldo=suel;

            Integer i1 = new Integer(42);
            Integer i2 = new Integer ("42");

           ///////////

            int id = i+1;
            String nombre = "cliente"+i;
            byte edad = (byte) (i+24);  // casting de la variable byte edad
            String fechaNacimiento = i+"02/02/1992";
            String email = "cliente@ocjp.com"+i;
            String tipoCuenta;
            if(i%2==0) {
                tipoCuenta=TipoCuenta.AHORROS.name();
            }else{
                tipoCuenta=TipoCuenta.CORRIENTE.name();
            }

            Cliente cliente = new Cliente(id,nombre,edad,fechaNacimiento,email,celular,sueldo,tipoCuenta);
            listaCliente.add(cliente);
            System.out.println(cliente.toString()+cliente.getTipoCuenta().toString());
        }
        return listaCliente;
    }
    
}
