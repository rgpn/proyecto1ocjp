package ocjpInterfaz;

/**
 * Esta interface nos permite manejar los metodos necesarios para simular un cajero y realizar transacciones:
 * abonar()
 * retirar()
 * consultar()
 *
 * @author Jonathan Pachacama
 * @version 13-06-2017
 */

public interface Transacciones {

    /**
     * Metodo que permite abonar una transaccion
     */

    public abstract  void abonar();

    /**
     * Metodo que permite realizar un retiro
     */
    public abstract  void retirar();

    /**
     * Metodo que permite consultar una transaccion
     */
    public abstract  void consultar();


}
